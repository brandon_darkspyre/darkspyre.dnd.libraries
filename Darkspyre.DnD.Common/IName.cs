﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Darkspyre.DnD.Interfaces
{
    public interface IName
    {
        string Name { get; set; }

        [NotMapped]
        public string NamedId
        {
            get
            {
                var n = (IName)this;
                if (n != null)
                {
                    return n.Name.ToLower().Replace(" ", "_").Replace("(", "").Replace(")", "");
                }
                return "";
            }
        }

        [NotMapped]
        public string FormatedName
        {
            get
            {
                var n = (IName)this;
                if (n != null)
                {
                    if (n.Name == "_index_")
                        return "(Index)";

                    var words = n.Name.ToLower().Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    var f = "";
                    foreach (var w in words)
                    {
                        f += w[0].ToString().ToUpper() + w.Substring(1) + " ";
                    }

                    return f.Trim();
                }
                return "";
            }
        }
    }
}