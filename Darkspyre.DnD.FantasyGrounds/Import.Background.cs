﻿using Darkspyre.DnD.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Darkspyre.DnD.FantasyGrounds
{
    public partial class Import
    {
        private async Task ImportBackgrounds(XmlNode backgroundNodes, DataLibrary dl)
        {
            await Task.Run(() =>
            {
                foreach (XmlNode bgdata in backgroundNodes.ChildNodes)
                {
                    if (bgdata.Name != "#whitespace" && bgdata.HasChildNodes)
                    {
                        var bg = new Background();
                        foreach (XmlNode c in bgdata.ChildNodes)
                        {
                            switch (bgdata.Name)
                            {
                                case "name":
                                    bg.Name = c.InnerText.Trim();
                                    break;
                                case "text":
                                    bg.Text = c.InnerXml.Trim();
                                    break;
                                case "skill":
                                    bg.Skill = c.InnerText.Trim();
                                    break;
                                case "languages":
                                    bg.Languages = c.InnerText.Trim();
                                    break;
                                case "equipment":
                                    bg.Equipment = c.InnerText.Trim();
                                    break;
                                case "supportingtables":
                                    bg.SupportingTables = c.InnerText.Trim();
                                    break;
                                case "features":
                                    foreach (XmlNode f in c.ChildNodes)
                                    {
                                        var fe = ImportFeature(f);
                                        bg.Features = new List<BackgroundFeature>();
                                        bg.Features.Add(new BackgroundFeature
                                        {
                                            Level = 0,
                                            Name = fe.Name,
                                            Text = fe.Text,
                                        });
                                    }
                                    break;
                            }
                        }
                        dl.Backgrounds.Add(bg);
                    }
                }
            });
        }
    }
}
