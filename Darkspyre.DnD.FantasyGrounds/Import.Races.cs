﻿using Darkspyre.DnD.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Darkspyre.DnD.FantasyGrounds
{
    public partial class Import
    {
        private async Task ImportRaces(XmlNode raceNode, DataLibrary dl)
        {
            await Task.Run(() =>
           {
               foreach (XmlNode racedata in raceNode.ChildNodes)
               {
                   if (racedata.Name != "#whitespace" && racedata.HasChildNodes)
                   {
                       Race race = new Race();
                       foreach (XmlNode c in racedata.ChildNodes)
                       {
                           if (c.Name != "#whitespace")
                           {
                               switch (c.Name)
                               {
                                   case "name":
                                       race.Name = c.InnerText.Trim();
                                       break;
                                   case "text":
                                       race.Text = c.InnerXml.Trim();
                                       break;
                                   case "traits":
                                       foreach (XmlNode cn in c.ChildNodes)
                                       {
                                           if (cn.Name != "#whitespace")
                                               race.Traits.Add(ImportNameValue(cn));
                                       }
                                       break;
                                   case "subraces":
                                       foreach (XmlNode cn in c.ChildNodes)
                                       {
                                           if (cn.Name != "#whitespace")
                                               race.SubRaces.Add(ImportNameValue(cn));
                                       }
                                       break;
                               }
                           }
                       }

                       dl.Races.Add(race);
                   }
               }
           });
        }
    }
}
