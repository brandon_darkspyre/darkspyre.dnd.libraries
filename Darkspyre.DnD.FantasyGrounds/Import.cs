﻿using Darkspyre.DnD.Data.Entities;
using Darkspyre.DnD.Interface;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml;

namespace Darkspyre.DnD.FantasyGrounds
{
    public partial class Import : IImportData
    {
        public async Task<DataLibrary> ImportFile(string dbPath, string definitionPath, int sourceType)
        {
            var dl = new DataLibrary();
            XmlDocument doc = new XmlDocument();
            doc.PreserveWhitespace = true;
            try
            {
                doc.Load(dbPath);
                XmlNode spellbookNode = null;
                XmlNode rootNode = null;
                XmlNode referenceNode = null;
                XmlNode NPCNode = null;
                XmlNode raceNode = null;
                XmlNode classNode = null;
                XmlNode bgNode = null;
                XmlNode itemNode = null;

                foreach (XmlNode n in doc.ChildNodes)
                {
                    if (n.Name == "root")
                    {
                        rootNode = n;
                        referenceNode = n;
                        break;
                    }
                }
                foreach (XmlNode n in rootNode.ChildNodes)
                {
                    if (n.Name == "reference")
                    {
                        referenceNode = n;
                        break;
                    }
                }

                //check root node for our data nodes - campaign mode
                if (rootNode != null)
                {
                    foreach (XmlNode n in rootNode.ChildNodes)
                    {
                        if (n.Name == "spell") //spelldata in modules db/client
                        {
                            spellbookNode = n;
                        }
                        if (n.Name == "npc")
                        {
                            NPCNode = n;
                        }
                    }
                }
                // if we haven't found our nodes, then they are probably in the reference node - module mode
                if (spellbookNode == null || NPCNode == null)
                {
                    foreach (XmlNode n in referenceNode.ChildNodes)
                    {
                        if (n.Name == "spelldata")
                        {
                            spellbookNode = n;
                        }
                        if (n.Name == "npcdata")
                        {
                            NPCNode = n;
                        }
                        if (n.Name == "racedata")
                        {
                            raceNode = n;
                        }
                        if (n.Name == "classdata")
                        {
                            classNode = n;
                        }
                        if (n.Name == "backgrounddata")
                        {
                            bgNode = n;
                        }
                        if (n.Name == "equipmentdata")
                        {
                            itemNode = n;
                        }
                    }
                }

                var tasks = new List<Task>();

                if (spellbookNode != null)
                {
                    tasks.Add(ImportSpells(spellbookNode, dl));
                }
                if (NPCNode != null)
                {
                    tasks.Add(ImportNPC(NPCNode, dl));
                }
                if (raceNode != null)
                {
                    tasks.Add(ImportRaces(raceNode, dl));
                }
                if (classNode != null)
                {
                    tasks.Add(ImportCharClasses(classNode, dl));
                }
                if (bgNode != null)
                {
                    tasks.Add(ImportBackgrounds(bgNode, dl));
                }
                if (itemNode != null)
                {
                    tasks.Add(ImportItems(itemNode, dl));
                }
                //add task to import charsheets

                //add task to import items
                //add task to import other things
                //Will need to check and see if async updating the same object but different lists will matter.

                await Task.WhenAll(tasks);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return dl;
        }

        private NameText ImportNameValue(XmlNode node)
        {
            var nv = new NameText();
            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.Name != "#whitespace")
                {
                    switch (n.Name.ToLower().Trim())
                    {
                        case "name":
                        case "group":
                            nv.Name = n.InnerText.Trim();
                            break;
                        case "text":
                        case "desc":
                        case "item":
                            nv.Text = n.InnerXml.Trim();
                            break;
                    }
                }
            }
            return nv;
        }

        private AbilityScore ImportAbilityScore(XmlNode data)
        {
            var a = new AbilityScore();
            switch (data.Name.ToLower())
            {
                case "strength":
                    a.AbilityType = AbilityType.Strength;
                    break;

                case "dexterity":
                    a.AbilityType = AbilityType.Dexterity;
                    break;

                case "constitution":
                    a.AbilityType = AbilityType.Constitution;
                    break;

                case "wisdom":
                    a.AbilityType = AbilityType.Wisdom;
                    break;

                case "intelligence":
                    a.AbilityType = AbilityType.Intelligence;
                    break;

                case "charisma":
                    a.AbilityType = AbilityType.Charisma;
                    break;
            }
            foreach (XmlNode n in data.ChildNodes)
            {
                if (n.Name != "#whitespace")
                {
                    switch (n.Name)
                    {
                        case "score":
                            a.Score = Convert.ToInt32(n.InnerText);
                            break;

                        case "bonus":
                            a.Bonus = Convert.ToInt32(n.InnerText);
                            break;
                    }
                }
            }
            return a;
        }

        private Feature ImportFeature(XmlNode node)
        {
            var nv = new Feature();
            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.Name != "#whitespace")
                {
                    switch (n.Name.ToLower().Trim())
                    {
                        case "name":
                            nv.Name = n.InnerText.Trim();
                            break;
                        case "text":
                        case "desc":
                            nv.Text = n.InnerXml.Trim();
                            break;
                        case "level":
                            nv.Level = Convert.ToInt32(n.InnerText.Trim());
                            break;
                    }
                }
            }
            return nv;
        }
    }
}