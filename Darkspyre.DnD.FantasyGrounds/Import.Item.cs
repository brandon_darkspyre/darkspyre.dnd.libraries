﻿using Darkspyre.DnD.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Xml;


namespace Darkspyre.DnD.FantasyGrounds
{
    public partial class Import
    {
        private async Task ImportItems(XmlNode itemNodes, DataLibrary dl)
        {
            //List<string> NodeName = new List<string>();
            await Task.Run(() =>
            {
                Dictionary<string, List<string>> SubItems = new Dictionary<string, List<string>>();
                foreach (XmlNode itemData in itemNodes.ChildNodes)
                {
                    if (itemData.Name != "#whitespace" && itemData.HasChildNodes)
                    {
                        EquipmentItem eq = new EquipmentItem();
                        eq.Id = itemData.Name;
                        foreach (XmlNode c in itemData.ChildNodes)
                        {
                            if (c.Name != "#whitespace")
                            {
                                //if (!NodeName.Contains(c.Name))
                                //{
                                //    NodeName.Add(c.Name);
                                //}
                                switch (c.Name)
                                {
                                    case "name":
                                        eq.Name = c.InnerText;
                                        break;

                                    case "type":
                                        eq.Type = c.InnerText;
                                        break;

                                    case "subtype":
                                        eq.Subtype = c.InnerText;
                                        break;

                                    case "cost":
                                        eq.Cost = c.InnerText;
                                        break;

                                    case "weight":
                                        eq.Weight = c.InnerText;
                                        break;

                                    case "description":
                                        eq.Text = c.InnerXml;
                                        break;

                                    case "subitems":
                                        //List<string> subitems = new List<string>();
                                        //foreach (XmlNode si in c.ChildNodes)
                                        //{
                                        //    if (si.Name != "#whitespace" && si.HasChildNodes)
                                        //    {                                                
                                        //        foreach (XmlNode si_n in si.ChildNodes)
                                        //        {
                                        //            if (si_n.Name != "#whitespace")
                                        //            {
                                        //                string name = "";
                                        //                string count = "";
                                        //                foreach (XmlNode si_an in si.ChildNodes)
                                        //                {
                                        //                    switch (si_an.Name)
                                        //                    {
                                        //                        case "name":
                                        //                            name = si_an.InnerText;
                                        //                            break;
                                        //                        case "count":
                                        //                            count = si_an.InnerText;
                                        //                            break;
                                        //                    }
                                        //                }
                                        //                subitems.Add(name + "|" + count);
                                        //            }
                                        //        }
                                        //        //SubItems.Add(eq.Name, name + "|" + count);
                                        //    }
                                        //    SubItems.Add(eq.Name, subitems);
                                        //}
                                        break;

                                    case "ac":
                                        eq.AC = c.InnerText;
                                        break;

                                    case "dexbonus":
                                        eq.DexBonus = c.InnerText;
                                        break;

                                    case "strength":
                                        eq.Strength = c.InnerText;
                                        break;

                                    case "stealth":
                                        eq.Stealth = c.InnerText;
                                        break;

                                    case "damage":
                                        eq.Damage = c.InnerText;
                                        break;

                                    case "properties":
                                        eq.Properties = c.InnerText;
                                        break;

                                    case "speed":
                                        eq.Speed = c.InnerText;
                                        break;

                                    case "carryingcapacity":
                                        eq.CarryingCapacity = c.InnerText;
                                        break;
                                }
                            }
                        }
                        dl.EquipmentItems.Add(eq);
                    }
                }

                //foreach(var nn in NodeName)
                //{
                //    Console.WriteLine("public string " + nn + " { get; set; }");
                //}
            });

         
        }
    }
}
