﻿using Darkspyre.DnD.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Darkspyre.DnD.FantasyGrounds
{
    public partial class Import
    {
        private async Task ImportCharClasses(XmlNode classesNode, DataLibrary dl)
        {
            await Task.Run(() =>
            {
                foreach (XmlNode classdata in classesNode.ChildNodes)
                {
                    if (classdata.Name != "#whitespace" && classdata.HasChildNodes)
                    {
                        CharClass ccl = new CharClass();
                        foreach (XmlNode c in classdata.ChildNodes)
                        {
                            if (c.Name != "#whitespace")
                            {
                                switch (c.Name)
                                {
                                    case "name":
                                        ccl.Name = c.InnerText.Trim();
                                        break;
                                    case "text":
                                        ccl.Text = c.InnerXml.Trim();
                                        break;

                                    case "hp":
                                        foreach (XmlNode cn in c.ChildNodes)
                                        {
                                            switch (cn.Name)
                                            {
                                                case "hitdice":
                                                    ccl.HitDice = ImportNameValue(cn).Text;
                                                    break;
                                                case "hitpointsat1stlevel":
                                                    ccl.HitDice1stLevel = ImportNameValue(cn).Text;
                                                    break;
                                                case "hitpointsathigherlevels":
                                                    ccl.HitDiceAdditional = ImportNameValue(cn).Text;
                                                    break;
                                            }
                                        }
                                        break;
                                    case "proficiencies":
                                        foreach (XmlNode cn in c.ChildNodes)
                                        {
                                            switch (cn.Name)
                                            {
                                                case "armor":
                                                    ccl.ProfArmor = ImportNameValue(cn).Text;
                                                    break;
                                                case "weapons":
                                                    ccl.ProfWeapons = ImportNameValue(cn).Text;
                                                    break;
                                                case "tools":
                                                    ccl.ProfTools = ImportNameValue(cn).Text;
                                                    break;
                                                case "savingthrows":
                                                    ccl.ProfSavingThrows = ImportNameValue(cn).Text;
                                                    break;
                                                case "skills":
                                                    ccl.ProfSkills = ImportNameValue(cn).Text;
                                                    break;
                                            }
                                        }
                                        break;
                                    case "multiclassproficiencies":
                                        foreach (XmlNode cn in c.ChildNodes)
                                        {
                                            if (cn.Name != "#whitespace")
                                            {
                                                NameText mcp = ImportNameValue(cn);
                                                ccl.MulticlassProficiencies.Add(new MulticlassProficiencies
                                                {
                                                    Name = mcp.Name,
                                                    Text = mcp.Text,
                                                });
                                            }
                                        }
                                        break;
                                    case "features":
                                        foreach (XmlNode cn in c.ChildNodes)
                                        {
                                            if (cn.Name != "#whitespace")
                                            {
                                                Feature f = ImportFeature(cn);
                                                ccl.ClassFeatures.Add(new ClassFeature
                                                {
                                                    Level = f.Level,
                                                    Name = f.Name,
                                                    Text = f.Text,
                                                });
                                            }
                                        }
                                        break;
                                    case "abilities":
                                        foreach (XmlNode cn in c.ChildNodes)
                                        {
                                            if (cn.Name != "#whitespace")
                                            {
                                                Feature f = ImportFeature(cn);
                                                ccl.ClassAbilities.Add(new ClassAbility
                                                {
                                                    Level = f.Level,
                                                    Name = f.Name,
                                                    Text = f.Text,
                                                });
                                            }
                                        }
                                        break;
                                    case "equipment":
                                        foreach (XmlNode cn in c.ChildNodes)
                                        {
                                            if (cn.Name != "#whitespace")
                                            {
                                                var nveq = ImportNameValue(cn);
                                                ccl.ClassEquipments.Add(new ClassEquipment
                                                {
                                                    Name = nveq.Name,
                                                    Text = nveq.Text,
                                                });
                                            }
                                        }
                                        break;
                                }
                            }
                        }

                        dl.CharacterClasses.Add(ccl);
                    }
                }
            });
        }
    }
}
