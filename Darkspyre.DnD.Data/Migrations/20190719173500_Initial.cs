﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Darkspyre.DnD.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Backgrounds",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Text = table.Column<string>(nullable: true),
                    Skill = table.Column<string>(nullable: true),
                    Languages = table.Column<string>(nullable: true),
                    Equipment = table.Column<string>(nullable: true),
                    SupportingTables = table.Column<string>(nullable: true),
                    SourceBook = table.Column<string>(nullable: true),
                    SourceBookAbr = table.Column<string>(nullable: true),
                    ImportDate = table.Column<DateTime>(nullable: false),
                    ImportSource = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Backgrounds", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CharacterClasses",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Text = table.Column<string>(nullable: true),
                    HitDice = table.Column<string>(nullable: true),
                    HitDice1stLevel = table.Column<string>(nullable: true),
                    HitDiceAdditional = table.Column<string>(nullable: true),
                    ProfArmor = table.Column<string>(nullable: true),
                    ProfWeapons = table.Column<string>(nullable: true),
                    ProfTools = table.Column<string>(nullable: true),
                    ProfSavingThrows = table.Column<string>(nullable: true),
                    ProfSkills = table.Column<string>(nullable: true),
                    SourceBook = table.Column<string>(nullable: true),
                    SourceBookAbr = table.Column<string>(nullable: true),
                    ImportDate = table.Column<DateTime>(nullable: false),
                    ImportSource = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharacterClasses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Races",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Text = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Races", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RecordLink",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    RecordClass = table.Column<string>(nullable: true),
                    RecordName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RecordLink", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Spells",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    FormId = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Level = table.Column<byte>(nullable: false),
                    School = table.Column<string>(nullable: true),
                    Ritual = table.Column<bool>(nullable: false),
                    Time = table.Column<string>(nullable: true),
                    Range = table.Column<string>(nullable: true),
                    Components = table.Column<string>(nullable: true),
                    Duration = table.Column<string>(nullable: true),
                    Classes = table.Column<string>(nullable: true),
                    Text = table.Column<string>(nullable: true),
                    Roll = table.Column<string>(nullable: true),
                    AreaOfEffect = table.Column<string>(nullable: true),
                    Locked = table.Column<bool>(nullable: false),
                    MAC = table.Column<int>(nullable: false),
                    PSPCost = table.Column<int>(nullable: false),
                    PSPFail = table.Column<int>(nullable: false),
                    Save = table.Column<string>(nullable: true),
                    Sphere = table.Column<string>(nullable: true),
                    ShortDescription = table.Column<string>(nullable: true),
                    SpellType = table.Column<string>(nullable: true),
                    SpellResistance = table.Column<string>(nullable: true),
                    Source = table.Column<string>(nullable: true),
                    SourceBook = table.Column<string>(nullable: true),
                    SourceBookAbr = table.Column<string>(nullable: true),
                    ImportDate = table.Column<DateTime>(nullable: false),
                    ImportSource = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Spells", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BackgroundFeature",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Text = table.Column<string>(nullable: true),
                    Level = table.Column<int>(nullable: false),
                    BackgroundId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BackgroundFeature", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BackgroundFeature_Backgrounds_BackgroundId",
                        column: x => x.BackgroundId,
                        principalTable: "Backgrounds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClassAbility",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Text = table.Column<string>(nullable: true),
                    Level = table.Column<int>(nullable: false),
                    CharClassId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassAbility", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClassAbility_CharacterClasses_CharClassId",
                        column: x => x.CharClassId,
                        principalTable: "CharacterClasses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClassEquipment",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Text = table.Column<string>(nullable: true),
                    CharClassId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassEquipment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClassEquipment_CharacterClasses_CharClassId",
                        column: x => x.CharClassId,
                        principalTable: "CharacterClasses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClassFeature",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Text = table.Column<string>(nullable: true),
                    Level = table.Column<int>(nullable: false),
                    CharClassId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassFeature", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClassFeature_CharacterClasses_CharClassId",
                        column: x => x.CharClassId,
                        principalTable: "CharacterClasses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MulticlassProficiencies",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Text = table.Column<string>(nullable: true),
                    CharClassId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MulticlassProficiencies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MulticlassProficiencies_CharacterClasses_CharClassId",
                        column: x => x.CharClassId,
                        principalTable: "CharacterClasses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CharacterSheets",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Holder = table.Column<string>(nullable: true),
                    Alignment = table.Column<string>(nullable: true),
                    Background = table.Column<string>(nullable: true),
                    BackgroundLinkId = table.Column<string>(nullable: true),
                    Bonds = table.Column<string>(nullable: true),
                    CoinOther = table.Column<string>(nullable: true),
                    CurHP = table.Column<int>(nullable: false),
                    CurrentEdit = table.Column<int>(nullable: false),
                    DCI = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Age = table.Column<int>(nullable: false),
                    Appearance = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharacterSheets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CharacterSheets_RecordLink_BackgroundLinkId",
                        column: x => x.BackgroundLinkId,
                        principalTable: "RecordLink",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AbilityScore",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AbilityType = table.Column<int>(nullable: false),
                    Bonus = table.Column<int>(nullable: false),
                    Save = table.Column<int>(nullable: false),
                    SaveModifer = table.Column<int>(nullable: false),
                    SaveProf = table.Column<int>(nullable: false),
                    Score = table.Column<int>(nullable: false),
                    CharSheetId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AbilityScore", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AbilityScore_CharacterSheets_CharSheetId",
                        column: x => x.CharSheetId,
                        principalTable: "CharacterSheets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CharSheetClass",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    CasterLevelInvMult = table.Column<string>(nullable: true),
                    HDDie = table.Column<string>(nullable: true),
                    HDUsed = table.Column<int>(nullable: false),
                    Level = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ShortcutId = table.Column<string>(nullable: true),
                    CharSheetId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharSheetClass", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CharSheetClass_CharacterSheets_CharSheetId",
                        column: x => x.CharSheetId,
                        principalTable: "CharacterSheets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CharSheetClass_RecordLink_ShortcutId",
                        column: x => x.ShortcutId,
                        principalTable: "RecordLink",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CoinSlot",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Amount = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    CharSheetId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoinSlot", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CoinSlot_CharacterSheets_CharSheetId",
                        column: x => x.CharSheetId,
                        principalTable: "CharacterSheets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AbilityScore_CharSheetId",
                table: "AbilityScore",
                column: "CharSheetId");

            migrationBuilder.CreateIndex(
                name: "IX_BackgroundFeature_BackgroundId",
                table: "BackgroundFeature",
                column: "BackgroundId");

            migrationBuilder.CreateIndex(
                name: "IX_CharacterSheets_BackgroundLinkId",
                table: "CharacterSheets",
                column: "BackgroundLinkId");

            migrationBuilder.CreateIndex(
                name: "IX_CharSheetClass_CharSheetId",
                table: "CharSheetClass",
                column: "CharSheetId");

            migrationBuilder.CreateIndex(
                name: "IX_CharSheetClass_ShortcutId",
                table: "CharSheetClass",
                column: "ShortcutId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassAbility_CharClassId",
                table: "ClassAbility",
                column: "CharClassId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassEquipment_CharClassId",
                table: "ClassEquipment",
                column: "CharClassId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassFeature_CharClassId",
                table: "ClassFeature",
                column: "CharClassId");

            migrationBuilder.CreateIndex(
                name: "IX_CoinSlot_CharSheetId",
                table: "CoinSlot",
                column: "CharSheetId");

            migrationBuilder.CreateIndex(
                name: "IX_MulticlassProficiencies_CharClassId",
                table: "MulticlassProficiencies",
                column: "CharClassId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AbilityScore");

            migrationBuilder.DropTable(
                name: "BackgroundFeature");

            migrationBuilder.DropTable(
                name: "CharSheetClass");

            migrationBuilder.DropTable(
                name: "ClassAbility");

            migrationBuilder.DropTable(
                name: "ClassEquipment");

            migrationBuilder.DropTable(
                name: "ClassFeature");

            migrationBuilder.DropTable(
                name: "CoinSlot");

            migrationBuilder.DropTable(
                name: "MulticlassProficiencies");

            migrationBuilder.DropTable(
                name: "Races");

            migrationBuilder.DropTable(
                name: "Spells");

            migrationBuilder.DropTable(
                name: "Backgrounds");

            migrationBuilder.DropTable(
                name: "CharacterSheets");

            migrationBuilder.DropTable(
                name: "CharacterClasses");

            migrationBuilder.DropTable(
                name: "RecordLink");
        }
    }
}
