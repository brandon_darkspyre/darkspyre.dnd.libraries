﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Darkspyre.DnD.Data.Migrations
{
    public partial class ClassTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClassEquipment_CharacterClasses_CharClassId",
                table: "ClassEquipment");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClassEquipment",
                table: "ClassEquipment");

            migrationBuilder.RenameTable(
                name: "ClassEquipment",
                newName: "ClassEquipments");

            migrationBuilder.RenameIndex(
                name: "IX_ClassEquipment_CharClassId",
                table: "ClassEquipments",
                newName: "IX_ClassEquipments_CharClassId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClassEquipments",
                table: "ClassEquipments",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ClassEquipments_CharacterClasses_CharClassId",
                table: "ClassEquipments",
                column: "CharClassId",
                principalTable: "CharacterClasses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClassEquipments_CharacterClasses_CharClassId",
                table: "ClassEquipments");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClassEquipments",
                table: "ClassEquipments");

            migrationBuilder.RenameTable(
                name: "ClassEquipments",
                newName: "ClassEquipment");

            migrationBuilder.RenameIndex(
                name: "IX_ClassEquipments_CharClassId",
                table: "ClassEquipment",
                newName: "IX_ClassEquipment_CharClassId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClassEquipment",
                table: "ClassEquipment",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ClassEquipment_CharacterClasses_CharClassId",
                table: "ClassEquipment",
                column: "CharClassId",
                principalTable: "CharacterClasses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
