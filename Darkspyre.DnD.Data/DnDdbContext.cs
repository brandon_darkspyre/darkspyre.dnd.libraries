﻿using Darkspyre.DnD.Data.Entities;

using Microsoft.EntityFrameworkCore;

namespace Darkspyre.DnD.Data
{
    public class DnDdbContext : DbContext
    {
        
        public DbSet<Background> Backgrounds { get; set; }
        public DbSet<CharClass> CharacterClasses { get; set; }

        //public virtual DbSet<Feature> CharClassFeatures { get; set; }
        //public virtual DbSet<Feature> CharClassAbilities { get; set; }
        //public virtual DbSet<NameText> CharClassEquipment { get; set; }

        public DbSet<CharSheet> CharacterSheets { get; set; }
        public DbSet<MulticlassProficiencies> MulticlassProficiencies { get; set; }
        public DbSet<ClassFeature> ClassFeature { get; set; }
        public DbSet<ClassAbility> ClassAbility { get; set; }
        public DbSet<ClassEquipment> ClassEquipments { get; set; }
        public DbSet<EquipmentItem> EquipmentItems { get; set; }
        public DbSet<Feat> Feats { get; set; }
        // public DbSet<NonPlayerCharacter> NonPlayerCharacters { get; set; }
        public DbSet<Race> Races { get; set; }
        public DbSet<Spell> Spells { get; set; }

        public DnDdbContext(DbContextOptions<DnDdbContext> options)
            : base(options)
        {
        }
    }
}