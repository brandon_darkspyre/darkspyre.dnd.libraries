﻿using Darkspyre.DnD.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Darkspyre.DnD.Data.Entities
{
    public class BaseEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public string Id { get; set; } = Guid.NewGuid().ToString();

        /* Both of these Name methods need to go into IName as a default implementation */

       
    }

    
}