﻿using Darkspyre.DnD.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Darkspyre.DnD.Data.Entities
{
    public class Race : BaseEntity, IName, IText
    {
        public string Name { get; set; } = "";
        public string Text { get; set; } = "";

        public List<NameText> Traits = new List<NameText>();
        public List<NameText> SubRaces = new List<NameText>();


    }
}
