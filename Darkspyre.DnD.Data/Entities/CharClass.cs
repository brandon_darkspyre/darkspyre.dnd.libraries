﻿using Darkspyre.DnD.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Darkspyre.DnD.Data.Entities
{
    public class CharClass : BaseEntity, IName, IText, ISource
    {
        public string Name { get; set; } = "";
        public string Text { get; set; } = "";
        public string HitDice { get; set; } = "";
        public string HitDice1stLevel { get; set; } = "";
        public string HitDiceAdditional { get; set; } = "";
        public string ProfArmor { get; set; } = "";
        public string ProfWeapons { get; set; } = "";
        public string ProfTools { get; set; } = "";
        public string ProfSavingThrows { get; set; } = "";
        public string ProfSkills { get; set; } = "";
        
        public virtual List<MulticlassProficiencies> MulticlassProficiencies { get; set; } = new List<MulticlassProficiencies>();

        public virtual List<ClassFeature> ClassFeatures { get; set; } = new List<ClassFeature>();

        public virtual List<ClassAbility> ClassAbilities { get; set; } = new List<ClassAbility>();

        public virtual List<ClassEquipment> ClassEquipments { get; set; } = new List<ClassEquipment>();
        public string SourceBook { get; set; }
        public string SourceBookAbr { get; set; }
        public DateTime ImportDate { get; set; }
        public string ImportSource { get; set; }
    }
}