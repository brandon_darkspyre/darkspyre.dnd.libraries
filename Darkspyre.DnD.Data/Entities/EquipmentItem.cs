﻿using Darkspyre.DnD.Data.Entities.Darkspyre.DnD.Data.CharSheetData;
using Darkspyre.DnD.Interfaces;
using System.Collections.Generic;


namespace Darkspyre.DnD.Data.Entities
{
    public class EquipmentItem : BaseEntity, IName, IText
    {
        public string Name { get; set; }       
        public string Type { get; set; }
        public string Subtype { get; set; }
        public string Cost { get; set; }
        public string Weight { get; set; }
        public string Text { get; set; }        
        public string AC { get; set; }
        public string DexBonus { get; set; }
        public string Strength { get; set; }
        public string Stealth { get; set; }
        public string Damage { get; set; }
        public string Properties { get; set; }
        public string Speed { get; set; }
        public string CarryingCapacity { get; set; }
    }
}
