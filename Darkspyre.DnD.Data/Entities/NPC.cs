﻿using Darkspyre.DnD.Interfaces;
using System.Collections.Generic;

namespace Darkspyre.DnD.Data.Entities
{
    public class NonPlayerCharacter : BaseEntity, IName
    {
        public List<AbilityScore> Abilities { get; set; } = new List<AbilityScore>();
        public int AC { get; set; }
        public string ACText { get; set; }
        public List<NameText> Actions { get; set; } = new List<NameText>();
        public string Alignment { get; set; }
        public string ConditionImmunities { get; set; }
        public string CR { get; set; }
        public string DamageImmunities { get; set; }
        public string DamageResistances { get; set; }
        public string DamageVulnerabilities { get; set; }
        public string HD { get; set; }
        public int HP { get; set; }
        public List<NameText> InnateSpells { get; set; } = new List<NameText>();
        public List<NameText> LairActions { get; set; } = new List<NameText>();
        public string Languages { get; set; }
        public List<NameText> LegendaryActions { get; set; } = new List<NameText>();
        public string Name { get; set; }
        public List<NameText> Reactions { get; set; } = new List<NameText>();
        public string SavingThrows { get; set; }
        public string Senses { get; set; }
        public string Size { get; set; }
        public string Skills { get; set; }
        public string Speed { get; set; }
        public List<NameText> Spells { get; set; } = new List<NameText>();
        public List<NameText> SpellSlots { get; set; } = new List<NameText>();
        public string Text { get; set; }
        public string Token { get; set; }
        public List<NameText> Traits { get; set; } = new List<NameText>();
        public string Type { get; set; }
        public int XP { get; set; }
        public int Locked { get; set; }
    }
}