﻿using Darkspyre.DnD.Interfaces;
using System;
using System.Collections.Generic;

namespace Darkspyre.DnD.Data.Entities
{
    public class Background : BaseEntity, IName, IText, ISource
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public string Skill { get; set; }
        public string Languages { get; set; }
        public string Equipment { get; set; }
        public List<BackgroundFeature> Features { get; set; } = new List<BackgroundFeature>();
        public string SupportingTables { get; set; }
        public string SourceBook { get; set; }
        public string SourceBookAbr { get; set; }
        public DateTime ImportDate { get; set; }
        public string ImportSource { get; set; }
    }
}