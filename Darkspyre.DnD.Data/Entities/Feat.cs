﻿using Darkspyre.DnD.Interfaces;
using System;
using System.Collections.Generic;

namespace Darkspyre.DnD.Data.Entities
{
    public class Feat : BaseEntity, IName, IText
    {
        public string Name { get; set; }
        public string Text { get; set; }
    }
}
