﻿using Darkspyre.DnD.Interfaces;

namespace Darkspyre.DnD.Data.Entities
{
    public class Feature : BaseEntity, IName
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public int Level { get; set; }
    }

    public class ClassFeature : Feature
    {
        public string CharClassId { get; set; }
        public virtual CharClass CharClass { get; set; }
    }

    public class ClassAbility : Feature {
        public virtual CharClass CharClass { get; set; }
        public string CharClassId { get; set; }
    }

    public class BackgroundFeature : Feature {
           public string BackgroundId { get; set; }
        public virtual Background Background { get; set; }
    }

}