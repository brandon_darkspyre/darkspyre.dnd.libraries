﻿using Darkspyre.DnD.Interfaces;
using Darkspyre.Extensions;
using System;

namespace Darkspyre.DnD.Data.Entities
{
    public partial class Spell : BaseEntity, ISource, IName, IText
    {
        public string FormId { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        public string School { get; set; }
        public bool Ritual { get; set; }
        public string Time { get; set; }
        public string Range { get; set; }
        public string Components { get; set; }
        public string Duration { get; set; }
        public string Classes { get; set; }
        public string Text { get; set; }
        public string Roll { get; set; }
        public string AreaOfEffect { get; set; }
        public bool Locked { get; set; }
        public int MAC { get; set; }
        public int PSPCost { get; set; }
        public int PSPFail { get; set; }
        public string Save { get; set; }
        public string Sphere { get; set; }
        public string ShortDescription { get; set; }
        public string SpellType { get; set; }
        public string SpellResistance { get; set; }
        public string Source { get; set; }
        public string SourceBook { get; set; }
        public string SourceBookAbr { get; set; }
        public DateTime ImportDate { get; set; }
        public string ImportSource { get; set; }

        public Spell() { }

        protected Spell(Spell source)
        {
            Util.CopyAll<Spell>(source, this);
            
        }

        public object Clone()
        {
            var s = new Spell(this);
            s.Id = Guid.NewGuid().ToString();
            return s;
        }
    }

    
}