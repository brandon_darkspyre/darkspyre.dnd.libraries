﻿using Darkspyre.DnD.Interfaces;

namespace Darkspyre.DnD.Data.Entities
{
    public class NameText : BaseEntity, IName, IText
    {
        public string Name { get; set; }
        public string Text { get; set; }
    }

    public class MulticlassProficiencies : NameText
    {
        public string CharClassId { get; set; }
        public virtual CharClass CharClass { get; set; }
    }

    public class ClassEquipment : NameText
    {
        public string CharClassId { get; set; }
        public virtual CharClass CharClass { get; set; }
    }

}