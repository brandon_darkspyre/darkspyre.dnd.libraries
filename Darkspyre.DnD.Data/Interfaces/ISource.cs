﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Darkspyre.DnD.Interfaces
{
    public interface ISource
    {
        public string SourceBook { get; set; }
        public string SourceBookAbr { get; set; }
        public DateTime ImportDate { get; set; }
        public string ImportSource { get; set; }
    }
}
