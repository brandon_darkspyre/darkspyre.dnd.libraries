﻿using Darkspyre.DnD.Data.Entities;
using System.Threading.Tasks;

namespace Darkspyre.DnD.Interface
{
    public interface IImportData
    {
        Task<DataLibrary> ImportFile(string dbPath, string definitionPath, int sourceType);
    }
}