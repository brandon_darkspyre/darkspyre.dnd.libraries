﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Darkspyre.DnD.Interfaces
{
    public interface IText
    {
        string Text { get; set; }
    }
}
